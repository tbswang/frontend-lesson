---
marp: true
theme: gaia
---

# css
含义: 层叠样式表 (Cascading Style Sheets，缩写为 CSS）
用法: html标签只是提供默认的浏览器样式, css用来控制样式的具体显示. 这也符合单一原则.样式具体指什么呢,就是颜色, 大小, 背景颜色, 边框, 动画, 布局, 等等.

---

## css语法
```css
h1 {
    color: red;
}
```
这就是一个最简单的css例子.
* h1叫做选择器
* color是属性
* red是属性值
* 所有的属性和属性值都包含在一个大括号`{ }`中.

---

## 先从内联css开始
```html
<header style="
  color: blue;
  font-size: 30px;
  background-color: yellow;
  border: 1px dashed red;
  display: inline;
  ">
  abc
</header>
```
我们把上面括号中的内容, 直接当作字符串, 写在html元素的style属性中, 这就是内联css

* 样式和内容耦合在一起. 
* 不推荐日常使用

---

## css文件
[例子🌰](example/index.html)

### 一个新标签 link
```html
  <link rel="stylesheet" href="index.css">
```

**推荐这种用法**

---

## 选择器?
选中这个元素, 对他进行操作. 不止是css可以用, js也可以用.

### 常用的选择器
#### 基本的
* 标签选择器:使用标签 `h1 { }`
* 类选择器: `box { }`
* ID 选择器 `#unique { }`
* 属性选择器 `a[title] { }`

---

#### 关系选择器
* 邻近兄弟元素选择器 A + B
* 邻近兄弟元素选择器 A + B
* 兄弟元素选择器 A ~ B
* 直接子元素选择器 A > B
* 后代元素选择器 A B

---

#### 伪选择器 (Pseudo)
* 伪类选择器 `a:active{}`. 所有伪类: https://developer.mozilla.org/zh-CN/docs/Web/CSS/Pseudo-classes
* 伪元素选择器: `a::after{}`. https://developer.mozilla.org/zh-CN/docs/Web/CSS/Pseudo-elements

---

## 盒子模型

用来标准化展示内容区域

![](assets/box-model.png)

---

### 标准盒子模型 
* width = content ; 
* height = content ;
  ![](assets/standard-box-model.png)

#### 外边距合并

---

### IE盒子模型
* width = content + padding-left + padding-right
* height = 同理
  ![](assets/alternate-box-model.png)

---

### 块级盒子vs内联盒子
块: 类似html中的块级元素
内联: 类似inline元素
  
* 块级 => 内联 : `display: inline`
* 内联 => 块级 : `display: block`

* 合二为一: `display: inline-block`
  外表是一个inline, 实际是一个block.(柯南: 喵喵喵?)

---

### 值和单位
* 大小: px, em, rem.用于width
* 百分比: 100%, 用于width, 
* 数字: 比如 `opacity: 0.6;`
* 图片: `background-image: url(star.png);`
* 计算值: `width: calc(100% - 32px)`

---

* 颜色:
  * 十六进制: 0Xffff00;
  * rgb: rgb(0, 0, 0)
  * rgba: rgba(0, 0, 0, 1)
  * hsl: hsl(188, 97%, 28%)
  * hsla: hsla(188, 97%, 28%, 0.8)
  * 字符: red, green
  
---

## 布局
[参考](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout/Floats)
* 正常布局流
* flex
* grid
* float: left, right, none, inherit
* position: absoulute, relative, static, fixed, sticky

---

## flex

居中布局:
两栏布局: 

---

## css的调试
### 选择一个元素
* 通过页面选择元素
* 通过devtool 的 element页面选择元素
* 通过devtool 的console选择元素

### 修改值
* 在devtool中element改变值
* 在属性编辑区改变值

---

## BFC
**隔绝内外部元素的布局影响**


---

## 作业
* 使用flex和float, 完善上次作业的布局
* 安装nodejs(12+版本), chrome, vscode.
* 预习一下javascript