# html语言
HTML（超文本标记语言——HyperText Markup Language）, 用了显示网页的内容.


## 认识一个html元素
![](assets/element.png)

> ## 练习
> 自己创建一个html元素

## 认识元素的属性
![](assets/element.png)
* 属性和属性之间要有空格.
* 属性值是引号

> ## 练习
> 创建一个链接到https://www.baidu.com的a标签

## 认识html文件
```html
<!DOCTYPE html> 
<!-- <!DOCTYPE html>: 声明文档类型.  -->
<html>
<!-- <html>元素 跟元素 -->
  <head>
    <meta charset="utf-8">
    <!-- 一些元数据属性 -->
    <title>我的测试站点</title>
    <!-- 设置页面标题 -->
  </head>
  <body>
  <!-- 包含所有显示在页面上的内容 -->
    <p>这是我的页面</p>
  </body>
</html>
```

## block元素和inline元素
### block
以块的形式展现. 自己独占一行. 
常见的元素: div, header, p, 

### inline
除了block,就是inline.
常见: a, span, em, img

## html中常用元素
### 显示相关
* div: 显示一块
* span: 显示几个文字
* p: 显示一段文字
* h1: 一号标题,同理, h2, h3,一直到h6
* img: 显示图片
  * src: 图片到地址
* video: 显示视频
  * src: 视频地址
* ol: 有序列表
* ul: 无序列表
* li: 列表项
  * 例如:
  ```html
    <ul>
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
    </ul>
  ```
* table: 表格
* tr: 表格行
* td: 表格中到每一个元素
* th: 表格头

## 用户输入
* input: 输入框
  * type
    * text: 输入文字
    * submit: 表单提交按钮
    * password: 密码
  * value: 绑定到值
  * placeholder: 空白的时候,显示到内容
* select: 下拉选择
* option: 下拉选项
  * 例子
  ```html
  <select name="pets" id="pet-select">
    <option value="cat">Cat</option>
    <option value="dog">Dog</option>
  </select>
  ```
* form: 表单
  ```html
  <form action="" method="get" class="form-example">
    <div class="form-example">
      <label for="name">Enter your name: </label>
      <input type="text" name="name" id="name" required>
    </div>
    <div class="form-example">
      <label for="email">Enter your email: </label>
      <input type="email" name="email" id="email" required>
    </div>
    <div class="form-example">
      <input type="submit" value="Subscribe!">
    </div>
  </form>
  ```


# 课后作业
1. 垂直水平居中的登录框: 用户名, 密码, 确认, 
2. 左右两栏布局: 左侧出菜单, 右侧是内容区域. 
3. 上下布局: 上面是时间, 课程类型, 老师名字的表单, 下面是一个表格.序号, 课程名字.

> 这些作业涉及到了css的知识, 可以自己先学习.

4. 预习css


# 参考资料
1. [mdn](https://developer.mozilla.org/zh-CN/docs/Web/HTML)
