---
marp: true
---

# javascript
简称js, 是一种主要在浏览器中运行的编程语言

## javascirpt和java什么关系
**没有关系**

---

## js vs es5 vs es6 vs esnext vs ECMAScript
![](assets/ecms.png)

* es => ECMAScript
* es5 => ECMAScript 第5版 2009
* es6 => ECMAScript 第6版 2015
* es7 => ECMAScript 第7版 2016
* esnext => ECMAScript 后面的版本 

---

## DOM
文件对象模型(Document Obejct Model)

* DOM1
* DOM2
* DOM3
  
## BOM
浏览器对象模型

主要能力:
* 控制浏览器: 窗口, 
* 获取浏览器信息
  * navigator
  * location
  * screen
  * performance
  * cookie
  * XMLHttpRequest

## 在html中引入js
* 内联
  ```js
  <script >
  var a = 1;
</script>
  ```
* 外联: `<script src="./index.js"></script>`


## 语言基础
编程语言的基本都是, 语法, 语句, 操作符,数据类型, 流程控制 ,内置功能.
### hello world
```js
console.log('hello world');
```

### 语法
* 区分大小写
* 标识符: 我们所能起的名字. 字母、下划线、$, 数字. 首字母不能数字.
* 注释: // 或者/* */

---

### 语句
```js
let sum = a + b; // 推荐加分号
```
```js
if(test){
  console.log('a')
}
```
---

## 什么是变量
存放数据的容器.既然是容器, 就可以改变容器里面的东西. 所以是变量
> 指向内存中一块地址

## 基本数据类型
* boolean
* number
* string
* null
* undefined
引用数据类型
* array
* object
* function

不常用类型
> * symbol
> * map
> * weekmap
> * set
> * weekset

## 动态类型
js是一种动态类型语言.不同于c/c++, java.

## 运算符

### 算数运算符
* +-*/


### 自增/自减
* ++
* --

### 赋值
* =
* +=, -= , *=, /=

### 比较运算符
* === , ==
* < > <= >=
  
> 计算错误: 0.1 + 0.2 === 0.3 ?

## 字符串

* 创建: 单引号, 双引号, 反引号
* 连接字符
* 获取第N个字符
* 大小写
* 替换
  
## 数组
类似列表的数据结构

* 创建
* 获取和修改第N个元素
* 获取数组长度
* 转换为字符串
* 添加和删除

## 条件

## 循环

## 函数
函数是第一等公民

## 事件

## json
javascript object notation

## 对象

## 原型对象

## 异步

## 浏览器端运行环境
Window
