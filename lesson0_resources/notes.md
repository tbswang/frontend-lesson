# 日常使用的资料
1. 学会使用搜索引擎: goole/bing
2. [js高级程序设计(红宝书)](https://github.com/shihyu/JavaScript)
4. [es6, 阮一峰](https://es6.ruanyifeng.com/)
5. [css权威指南](http://gdut_yy.gitee.io/doc-csstdg4)
6. [mdn文档](https://developer.mozilla.org/zh-CN/)
7. [react官方文档](https://zh-hans.reactjs.org/)


# 开发环境搭建
* 浏览器: 推荐使用chrome或者firefox
* 文本编辑器: 推荐vscode
* nodejs: [下载地址](https://nodejs.org/zh-cn/download/) 网上有安装教程.